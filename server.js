const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const mongooseSetup = require("./server/mongooseSetup");
const graphqlSetup = require("./server/graphqlSetup");
const routes = require("./server/routes");
const { configureEnvironment } = require("./server/env-config");
const Scheduler = require("./server/scheduler/scheduler");

configureEnvironment();
mongooseSetup();

const app = express();
app.use(cors());
graphqlSetup(app);

app.use("/", routes);

const port = process.env.VUE_APP_GRAPHQL_PORT || 5000;
const host = `http://localhost:${port}`;
new Scheduler(host);

app.listen(port, () => {
	console.log(`Server listening on port ${port}`);
});
