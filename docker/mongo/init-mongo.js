// Create database and switch to it
db = db.getSiblingDB("falacaboo");

// Create user if doesn't exist
let userExists = db.getUser("developer");
if (!userExists) {
	db.createUser({
		user: "developer",
		pwd: "develop",
		roles: [
			{
				role: "readWrite",
				db: "falacaboo"
			}
		]
	});
	print("User 'developer' created successfully");
}

// Array of collection names
const collections = [
	"articles",
	"balance_versions",
	"balances",
	"members",
	"receipt_versions",
	"receipts"
];

// Function to convert Extended JSON ObjectId
function convertObjectIds(doc) {
	if (doc._id && doc._id.$oid) {
		doc._id = ObjectId(doc._id.$oid);
	}
	return doc;
}

// Function to import JSON data if file exists
function importJsonData(collectionName) {
	try {
		print(`Attempting to import data for ${collectionName}`);
        
		// Check if the file exists by attempting to load it
		let data = cat(`/docker-entrypoint-initdb.d/data/falacaboo.${collectionName}.json`);
		if (data) {
			// Parse JSON data
			let jsonData = JSON.parse(data);
            
			// If data is not an array, wrap it in an array
			if (!Array.isArray(jsonData)) {
				jsonData = [jsonData];
			}
            
			// Convert ObjectIds and prepare documents
			let processedDocuments = jsonData.map((doc, index) => {
				try {
					let convertedDoc = convertObjectIds(doc);
					return convertedDoc;
				} catch (convError) {
					print(`Error converting document ${index} in ${collectionName}:`);
					printjson(doc);
					print(`Error: ${convError.message}`);
					return null;
				}
			}).filter(doc => doc !== null);
            
			// Insert documents
			if (processedDocuments.length > 0) {
				try {
					db[collectionName].insertMany(processedDocuments, { ordered: false });
					print(`Successfully imported ${processedDocuments.length} documents into ${collectionName}`);
				} catch (bulkError) {
					// If some documents were inserted successfully, MongoDB will still return an error
					if (bulkError.hasWriteErrors()) {
						print(`Partial success importing to ${collectionName}. Some documents may have failed:`);
						bulkError.getWriteErrors().forEach(error => {
							print(`Document at index ${error.index} failed: ${error.errmsg}`);
						});
					} else {
						print(`Failed to import documents to ${collectionName}:`);
						printjson(bulkError);
					}
				}
			} else {
				print(`No valid documents found to import for ${collectionName}`);
			}
		}
	} catch (e) {
		if (e.message.includes("couldn't open file")) {
			print(`No import file found for ${collectionName}, skipping import`);
		} else {
			print(`Error processing data for ${collectionName}:`);
			printjson(e);
		}
	}
}

// Create collections and import data
collections.forEach(collectionName => {
	print(`Processing collection: ${collectionName}`);
    
	// Check if collection exists
	if (!db.getCollectionNames().includes(collectionName)) {
		try {
			// Create collection
			db.createCollection(collectionName);
			print(`Created collection: ${collectionName}`);
            
			// Import data if available
			importJsonData(collectionName);
		} catch (error) {
			print(`Error creating collection ${collectionName}:`);
			printjson(error);
		}
	} else {
		print(`Collection ${collectionName} already exists, skipping creation`);
	}
});

print("Database initialization completed");