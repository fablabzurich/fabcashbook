import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import "./plugins/vuetify-money.js";
import "material-design-icons-iconfont/dist/material-design-icons.css";
import { apolloProvider } from "./graphql/apollo.client";
import NotifyPlugin from "vue-easy-notify";
import "vue-easy-notify/dist/vue-easy-notify.css"; // import style
import WebCam from "vue-web-cam";

Vue.use(NotifyPlugin);
Vue.use(WebCam);
Vue.config.productionTip = false;

const moment = require("moment");
require("moment/locale/de");
Vue.use(require("vue-moment"), {
	moment
});

new Vue({
	router,
	store,
	vuetify,
	apolloProvider,
	render: h => h(App)
}).$mount("#app");
