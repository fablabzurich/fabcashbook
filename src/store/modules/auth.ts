
/**
 * all we need to handle login
 */

import { Module } from "vuex";

const auth: Module<any, any> = {
	namespaced: true,
	state: {
		loggedIn: false,
		labManager: "",
		lastLabManager: ""
	},
	getters: {
		isLoggedIn(state) {
			state.loggedIn = (localStorage.getItem("loggedIn") === "true");
			return state.loggedIn && state.labManager.length > 3;
		},
		getLabManager(state) {
			state.labManager = localStorage.getItem("labManager") ?? "";
			return state.labManager;
		},
		getLabManagerFirstName(state) {
			return state.labManager.split(" ")[0];
		},
		getLabManagerInitials(state) {
			if (!state.loggedIn) return "";

			const names = state.labManager.split(" ");
			return names.reduce(
				(letters: string | any[], name: string) => letters.concat(name.substr(0, 1))
				, ""
			);
		},
		getLastLabManager(state) {
			state.labManager = localStorage.getItem("lastLabManager") ?? "";
			return state.lastLabManager;
		}
	},
	mutations: {
		login(state) {
			state.loggedIn = true;
			localStorage.setItem("loggedIn", state.loggedIn);
		},
		logout(state) {		
			state.loggedIn = false;
			localStorage.setItem("lastLabManager", state.labManager);
			state.lastLabManager = state.labManager;
			state.labManager = "";
			localStorage.setItem("loggedIn", state.loggedIn);
			localStorage.setItem("labManager", "");
			console.log("**** logout ****");
		},
		setLabManager(state, name) {
			state.labManager = name;
			if( name === null) {
				localStorage.removeItem("labManager");
				return;
			}
			localStorage.setItem("labManager", name);
		},
		setLastLabManager(state, name) {
			state.labManager = name;
			localStorage.setItem("lastLabManager", name);
		}
	},
	actions: {
		login (context) {
			context.commit("login");
			context.rootState.postBooking = false;
			context.commit("setPostBooking", false, { root: true });
			context.dispatch("basket/clearCashBookEntry", null, { root: true });
		},
		logout (context) {
			let lastLabManager = context.state.labManager;
			context.commit("logout");
			context.commit("setPostBooking", false, { root: true });
			context.dispatch("basket/clearCashBookEntry", null, { root: true });
			localStorage.clear();
			console.log("context.commit(\"setLastLabManager\", false, { root: true });");
			context.commit("setLastLabManager", lastLabManager);
		}
	}
};

export default auth;
