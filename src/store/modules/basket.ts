/**
 * All we need to handle the articles that were used
 */

import { Module } from "vuex";
import { emptyArticle, Article } from "./article";

export interface Basket {
    [index: string]: Article;
}

export interface Member {
    memberId: number;
    firstname: string;
    lastname: string;
    fullname: string;
    fullnameAndEmail: string;
    email: string;
    address: string;
    city: string;
    postcode: string;
	paid: boolean;
    updatedAt: string;
    memberFee: number;
}

export interface Receipt {
    uuid: string;
    version: number;
    date: Date;
    labmanager: string;
    remark: string;
    basket: Basket;
    member?: Member;
    total: number;
    paymentMethod: string;
}

const basket: Module<any, any> = {
	namespaced: true,
	state: {
		receipt: null as Receipt | null,
		basket: [] as Article[],
		selectedArticle: { ...emptyArticle } as Article,
		checkoutDialog: false,
		member: null as Member | null
	},
	getters: {
		getBasket(state) {
			return state.basket.filter((item: Article) => !item.removed);
		},
		getBasketMapped(state, getters) {
			return getters.getBasket.map((item: Article) => ({
				key: item.key,
				name: item.name,
				description: item.description,
				price: +item.price,
				unit: item.unit,
				type: item.type,
				units: +item.units,
				konto: item.konto
			}));
		},
		getSelectedArticle(state) {
			return state.selectedArticle;
		},
		getSelectedArticleTotal(state) {
			const total = state.selectedArticle.price * state.selectedArticle.units;
			return total || 0;
		},
		getSelectedArticleTotalFormatted(state, getters) {
			const formatter = new Intl.NumberFormat("de-CH", {
				minimumFractionDigits: 2,
				maximumFractionDigits: 2
			});
			return formatter.format(Math.abs(getters.getSelectedArticleTotal));
		},
		getBasketTotal(state, getters) {
			return getters.getBasket.reduce((total: number, item: Article) => (item.units * item.price) + total, 0);
		},
		isCheckoutDialogOpen(state) {
			return state.checkoutDialog;
		},
		getMember(state) {
			return state.member;
		},
		getMemberMapped(state) {
			return {
				memberNumber: state.member?.memberNumber ?? null,
				firstname: state.member?.firstname ?? "",
				lastname: state.member?.lastname ?? "",
				email: state.member?.email ?? "",
				address: state.member?.address ?? "",
				city: state.member?.city ?? "",
				postcode: state.member?.postcode ?? "",
				memberFee: state.member?.memberFee ?? null
			};
		},
		getReceipt(state, getters, rootState, rootGetters) {
			const labmanager = rootGetters["auth/getLabManager"];
			const date = new Date();
			const remark = getters.getBookingRemark;
			const basket = getters.getBasketMapped;
			const member = getters.getMemberMapped;
			const total = getters.getBasketTotal;

			if (getters.isEditReceipt) {
				return {
					uuid: state.receipt!.uuid,
					date: state.receipt!.date,
					labmanager: state.receipt!.labmanager,
					remark: `${getters.getBookingRemark}${state.receipt!.remark}`,
					basket,
					member,
					total,
					paymentMethod: state.receipt!.paymentMethod
				};
			}
			return {
				date,
				labmanager,
				remark,
				basket,
				member,
				total
			};
		},
		getBookingRemark(state, getters, rootState, rootGetters) {
			const timestamp = new Date().toISOString();
			const labmanager = rootGetters["auth/getLabManager"];
			if (getters.isEditReceipt) {
				return `-[EDITED: ${timestamp} by ${labmanager}] `;
			}
			if (rootState.postBooking) {
				return `-[POSTBOOKED: ${timestamp}] `;
			}
			return "";
		},
		isEditReceipt(state) {
			return state.receipt != null && state.receipt.uuid?.length > 0;
		},
		isExpense(state, getters) {
			const hasExpenseArticle = state.basket.some((item: Article) => 
				item.key && item.key.startsWith("Expense")
			);
			return getters.getBasketTotal < 0 || hasExpenseArticle;
		},
		isIncome(state, getters) {
			const hasExpenseArticle = state.basket.some((item: Article) => 
				item.key && item.key.startsWith("Expense")
			);
			const hasIncomeArticle = state.basket.some((item: Article) => 
				item.key && !item.key.startsWith("Expense")
			);
			return !hasExpenseArticle && hasIncomeArticle;
		}
	},
	mutations: {
		addToBasket(state, item: Article) {
			state.basket.forEach((item: Article) => {
				item.selected = false;
			});
			item.selected = true;

			if (item.basketPos == null || item.basketPos < 0) {
				const newItem = { ...item };
				newItem.basketPos = state.basket.length;
				state.basket.push(newItem);
				state.selectedArticle = newItem;
			} else {
				state.basket.splice(item.basketPos, 1, item);
			}
			state.basket = [...state.basket];
		},
		removeFromBasket(state, pos: number) {
			state.basket[pos].removed = true;
			state.selectedArticle = { ...emptyArticle };
			state.basket = [...state.basket];
		},
		resetReceiptTo(state, editReceipt: Receipt) {
			state.selectedArticle = { ...emptyArticle };
			state.checkoutDialog = false;
			state.receipt = editReceipt;
			state.basket = Object.values(editReceipt.basket).map((item: Article, index: number) => ({
				selected: item.selected,
				key: item.key,
				name: item.name,
				description: item.description,
				price: item.price,
				unit: item.unit,
				type: item.type,
				konto: item.konto,
				units: item.units,
				basketPos: index
			}));
			
		},
		setSelectedArticle(state, item: Article) {
			if (state.selectedArticle) {
				state.selectedArticle.selected = false;
			}
			state.selectedArticle = { ...item };
			state.basket = [...state.basket];
		},
		editSelectedArticle(state, item: Article) {
			if (state.selectedArticle) {
				state.selectedArticle.selected = false;
			}
			state.selectedArticle = item;
			item.selected = true;
			state.basket = [...state.basket];
		},
		resetSelectedArticle(state) {
			state.basket.forEach((item: Article) => {
				item.selected = false;
			});
			state.basket = [...state.basket];
			state.selectedArticle = { ...emptyArticle };
		},
		incrementArticleUnits(state) {
			if (state.selectedArticle.units != null) {
				state.selectedArticle.units++;
			}
			state.basket = [...state.basket];
		},
		decrementArticleUnits(state) {
			if (state.selectedArticle.units != null && state.selectedArticle.units > 1) {
				state.selectedArticle.units--;
			}
		},
		setCheckoutDialog(state, open: boolean) {
			state.checkoutDialog = open;
		},
		setMember( state, member: Member) {
			state.member = member;
		},
		clearMember( state ) {
			state.member = null;
		},
		clearCashBookEntry(state) {
			state.receipt = null;
			state.selectedArticle = { ...emptyArticle };
			state.basket = [];
			state.member = null;
			state.checkoutDialog = false;
			console.log("cash book entry cleared");
		}
	},
	actions: {
		clearCashBookEntry(context) {
			context.commit("clearCashBookEntry");
		}
	}
};

export default basket;
