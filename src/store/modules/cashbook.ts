/**
 * all we need to handle the cashbook
 * the cashbook contain all saved booking transactions
 */

import { Receipt } from "./basket";

export interface CashBook {
	[index: string]: Receipt;
}


export default {
	state: {
		cashbook: new Array<Receipt>()
	}
};
