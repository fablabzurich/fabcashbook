/**
 * all we need to handle the articles
 * like machine use or materials
 */

import { Module } from "vuex";

const articles:Article[] = [];

export interface ArticledescriptionRule {
	label: String;
	placeholder: String;
	requiredMsg: String;
	regex: String;
}

export interface Article {
	selected: boolean;
	key: String;
	name: String;
	description?: String;
	price: number;
	unit: String; // Einheit
	type: String;
	konto: String;
	units: number;	// Anzahl Einheiten
	removed: boolean;
	basketPos?: number;
	descriptionRule?: ArticledescriptionRule

}

export interface ArticleGroup {
	name: String,
	items: Article[]
}

export const emptyArticle:Article = {
	selected: true,
	key: "",
	name: "",
	description: "",
	price: 0.0,
	unit: "",
	type: "",
	konto: "",
	units: 0,
	basketPos: -1,
	removed: false
};

const emptyArticleGroups: ArticleGroup[] = [];

const article: Module<any, any> = {
	namespaced: true,
	state: {
		articleGroups: emptyArticleGroups
	},
	getters: {
		getArticleGroups(state) {
			return state.articleGroups;
		}
	},
	mutations: {
		setArticleGroups(state, articleGroups) {
			state.articleGroups = articleGroups;
		}
	},
	actions: {
		fetchArticleGroups(context, payment) {
			const kontoPrefix = payment ? "3" : "4";
			return new Promise((resolve, reject) => {
				var articleGroups: ArticleGroup[] = [];
				articles.forEach((item: Article) => {
					if( item.konto.charAt(0) !== kontoPrefix ) { 
						return; 
					}
					var articleGroup = articleGroups.find(group => group.name === item.type);
					if (!articleGroup) {
						articleGroup = { name: item.type, items: [] as Article[] };
						articleGroups.push(articleGroup);
					}
					articleGroup.items.push(item);
				});
				context.commit("setArticleGroups", articleGroups);
				resolve(articleGroups);
			});
		}
	}
};

export default article;
