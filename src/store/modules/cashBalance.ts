/**
 * all we need for counting the amount of cash in our cashbox
 */

import { Module } from "vuex";
import { Decimal } from "decimal.js";

export interface CashBalanceCount {
	value: number;
	amount: number;
	sum: number;
	copySumToAmount: () => void;
	copyAmountToSum: () => void;
	getSumFromAmount: () => number;
	getAmountFromSum: () => number;
	isValidSum: () => boolean;
}

const emptyCash = [
	5,
	2,
	1,
	.5,
	.2,
	.1,
	.05,
	10,
	20,
	50,
	100,
	200
];

class CashBalancePrototype implements CashBalanceCount {
	value: number;
	amount: number;
	sum: number;

	constructor (value: number, amount: number, sum: number) {
		this.value = value;
		this.amount = amount;
		this.sum = sum;
	}
	copySumToAmount() {
		this.amount = this.getAmountFromSum();
	};
	copyAmountToSum() {
		this.sum = this.getSumFromAmount();
	};
	getAmountFromSum() {
		return Math.floor(Math.round((this.sum*100) / (this.value)) / 100);
	}
	getSumFromAmount() {
		return Math.round(this.amount * (this.value*100)) / 100;
	}
	isValidSum() {
		let sum = new Decimal(this.sum);
		let value = new Decimal(this.value);
		
		return sum.mod(value).equals(0);
	}
}


const cashBalance: Module<any, any> = {
	namespaced: true,
	state: {
		cashBalance: 0,
		cashBalanceCount: []
	},
	getters: {
		getCashBalanceCount(state, getters) {
			if (state.cashBalanceCount === null || state.cashBalanceCount.length === 0) {
				return getters.getEmptyCashBalanceCount;
			}

			return state.cashBalanceCount;
		},
		getEmptyCashBalanceCount(state) {
			const empty = emptyCash.map(e => new CashBalancePrototype( e, 0, 0 ));
			state.cashBalanceCount = empty;
			return empty;
		},
		getCashBalanceByAmount(state, getters) {
			return getters.getCashBalanceCount.reduce(
				(
					total:number, money:CashBalanceCount
				) => money.value * money.amount + total
				, 0);
		},
		getCashBalanceBySum(state, getters) {
			return getters.getCashBalanceCount.reduce(
				(
					total:number, money:CashBalanceCount
				) => 1 * money.sum + total
				, 0);
		}
	},
	mutations: {
		setCashBalanceCount(state, cashBalanceCount) {
			state.cashBalanceCount = cashBalanceCount;
		}
	}
};
export default cashBalance;

