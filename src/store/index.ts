import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";

import auth from "./modules/auth";
import article from "./modules/article";
import basket from "./modules/basket";
import cashbook from "./modules/cashbook";
import cashBalance from "./modules/cashBalance";

Vue.use(Vuex);

interface WindowSize {
	width: number;
	height: number;
}


export default new Vuex.Store({
	plugins: [
		createPersistedState({
			storage: window.sessionStorage
		})
	],
	modules: {
		auth,
		article,
		basket,
		cashbook,
		cashBalance
	},
	state: {
		serverAppVersion: process.env.PACKAGE_VERSION,
		currentTime: new Date(),
		idleSince: new Date(),
		bookingDate: new Date(),
		postBooking: false,
		windowSize: <WindowSize> { width: 1024, height: 768 }

	},
	actions: {},
	getters: {
		getServerAppVersion: state => state.serverAppVersion,
		getClientAppVersion: state => process.env.PACKAGE_VERSION,
		isNewAppVersion: (state,getter) => state.serverAppVersion !== getter.getClientAppVersion,
		getBookingDate(state) {
			return state.postBooking ? new Date(state.bookingDate) : new Date(state.currentTime);
		},
		getPostBooking(state) {
			return state.postBooking;
		},
		getWindowSize(state) {
			return state.windowSize;
		},
		getIdleSince(state) {
			return state.idleSince;
		}
	},
	mutations: {
		setServerAppVersion( state, value) {
			state.serverAppVersion = value;
			localStorage.setItem("serverAppVersion", state.serverAppVersion);
		},
		setCurrentTime(state, value) {
			state.currentTime = value;
		},
		setIdleSince(state, value) {
			state.idleSince = value;
		},
		setBookingDate(state, value) {
			state.bookingDate = value;
		},
		setPostBooking(state, value) {
			state.postBooking = value;
		},
		setWindowSize(state, windowSize) {
			state.windowSize = windowSize;
		}
	}
});
