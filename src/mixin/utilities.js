export default {
	methods: {
		priceFormatter(price) {
			const formatter = new Intl.NumberFormat("de-CH", {
				minimumFractionDigits: 2,
				maximumFractionDigits: 2
			 });
			return `CHF ${formatter.format(Math.abs(price))}`;
		},
		dateFormatted(inputDate, short = false, dateonly = false) {
			const date = new Date(inputDate);

			// Check if the date is valid
			if (isNaN(date.getTime())) {
			  // Handle invalid date (e.g., return a default value or throw an error)
			  return "Invalid date"; // Example: returning a default value
			}

			const optionsDate = {
				weekday: short ? "short" : "long",
				year: "numeric",
				month: short ? "short" : "long",
				day: "numeric"
			};
			const optionsTime = {
				hour: "numeric",
				minute: "numeric",
				second: "numeric"
			};
			const options = { ...optionsDate, ...(dateonly ? {} : optionsTime)};
			return date.toLocaleDateString("de-DE", options);
		},
		isSameDay(date1, date2) {
			return date1.getFullYear() == date2.getFullYear() &&
				date1.getMonth() == date2.getMonth() &&
				date1.getDate() == date2.getDate();
		},
		isToday(date) {
			return this.isSameDay(date, new Date());
		},
		isTodayString(dateString) {
			return this.isToday(this.$moment(dateString).toDate());
		},
		focusNext(e) {
			const type = e.target.type;
			const inputs = Array.from(e.target.form.querySelectorAll(`input[type=${type}]`));
			const index = inputs.indexOf(e.target);

			if (index < inputs.length - 1) {
				inputs[index + 1].focus();
			} else {
				inputs[0].focus();
			}
		},
		getDimensions() {
			function detectMob() {
			  const toMatch = [
					/Android/i,
					/webOS/i,
					/iPhone/i,
					/iPad/i,
					/iPod/i,
					/BlackBerry/i,
					/Windows Phone/i
			  ];
	  
			  return toMatch.some((toMatchItem) =>
					toMatchItem.test(navigator.userAgent)
			  );
			}
			const mobDevice = detectMob();
			console.log(this.width, this.height);
			return mobDevice
			  ? { width: this.height, height: this.width }
			  : { width: this.width, height: this.height };
		}		
	}
};
