import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "../views/Home.vue";
import store from "../store";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
	{
		path: "/",
		name: "Home",
		component: Home
	},
	{
		path: "/cashdesk",
		name: "Kasse",
		component: () =>
			import("../views/CashDesk.vue")
	},
	{
		path: "/expenses",
		name: "Ausgaben",
		component: () =>
			import("../views/Expenses.vue")
	},
	{
		path: "/membersStatus",
		name: "MembersStatus",
		component: () =>
			import("../components/Members.vue")
	},
	{
		path: "/members",
		name: "Members",
		component: () =>
			import("../components/MemberPayments.vue")
	},
	{
		path: "/admin",
		name: "Admin",
		component: () =>
			import("../views/Admin.vue")
	},
	{
		path: "/about",
		name: "About",
		component: () =>
			import("../views/About.vue")
	}

];

const router = new VueRouter({
	mode: "history",
	base: process.env.BASE_URL,
	routes
});

router.beforeEach((to, from, next) => {
	const _store = store;

	let isAuthenticated = _store.getters["auth/isLoggedIn"] || false;
	let isExpense = _store.getters["basket/isExpense"];
	let isIncome = _store.getters["basket/isIncome"];

	console.log(`loggedIn ${isAuthenticated} route to -> `, to);
	if (to.name !== "Home" && !isAuthenticated) {
		next({ name: "Home" });
	}
	else if( to.path === "/cashdesk" && isExpense) {	
		next({ path: "/expenses" });
	}
	else if( to.path === "/expenses" && isIncome) {	
		next({ path: "/cashdesk" });
	}
	else {
		next();
	}
});

export default router;
