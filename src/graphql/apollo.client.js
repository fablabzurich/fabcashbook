import Vue from "vue";
import router from "../router";
import { ApolloClient } from "apollo-client";
import { HttpLink } from "apollo-link-http";
import { ApolloLink } from "apollo-link";
import { InMemoryCache } from "apollo-cache-inmemory";
import { onError } from "apollo-link-error";
import VueApollo from "vue-apollo";
import find from "lodash/find";
import ApolloLinkTimeout from "apollo-link-timeout";

const timeoutLink = new ApolloLinkTimeout(20000);
const graphqlPort = process.env.VUE_APP_GRAPHQL_PORT || 5000;
const graphqlPath = process.env.VUE_APP_GRAPHQL_PATH || "/graphql";
const graphqlBase = process.env.VUE_APP_GRAPHQL_BASE || "http://localhost";
const graphqlEndpoint = `${graphqlBase}:${graphqlPort}${graphqlPath}`;
console.log(`graphqlEndpoint: ${graphqlEndpoint}`);
const httpLink = new HttpLink({
	uri: graphqlEndpoint
});

const timeoutHttpLink = timeoutLink.concat(httpLink);

const authMiddleware = new ApolloLink((operation, forward) => {
	// add the authorization to the headers
	const token = localStorage.getItem("tkn");
	operation.setContext({
		headers: {
			authorization: token ? `${token}` : null
		}
	});

	return forward(operation);
});

// Error Handling
const errorLink = onError(({ graphQLErrors, networkError }) => {
	if (graphQLErrors) {
		if (find(graphQLErrors, { message: "Not authenticated" })) {
			console.log("NO AUTH");
			router.push({
				name: "login",
				query: { redirectFrom: router.history.current.fullPath }
			});
		} else {
			graphQLErrors.map(({ message, locations, path }) =>
				console.log(
					`[Apollo Client Agent]: Message: ${message}, Location: ${locations}, Path: ${path}`
				)
			);
		}
	}
	if (networkError) {
		console.log(`[Apollo Client Agent] [Network]: ${networkError}`);
	}
});

// Create the apollo client
export const apolloClient = new ApolloClient({
	resolvers: {},
	link: authMiddleware.concat(errorLink.concat(timeoutHttpLink)),
	cache: new InMemoryCache(),
	connectToDevTools: true
});

// Install the Vue plugin

Vue.use(VueApollo);

export const apolloProvider = new VueApollo({
	defaultClient: apolloClient
});
