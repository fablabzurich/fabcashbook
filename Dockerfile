FROM node:16

# Create app directory
WORKDIR /app

# update npm
RUN npm update && npm -g install pm2

# copy all files to app source
COPY . /app

# install dependencies
RUN npm install

# Build static vue files
RUN npm run build

EXPOSE 8080
EXPOSE 5000

CMD [ "pm2-runtime", "start", "ecosystem.config.js" ]