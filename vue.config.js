const fs = require("fs");
const packageJson = fs.readFileSync("./package.json");
const version = JSON.parse(packageJson).version || 0;
var webpack = require("webpack");

module.exports = {
	configureWebpack: {
		plugins: [
			new webpack.DefinePlugin({
				"process.env": {
					PACKAGE_VERSION: "\"" + version + "\""
				}
			})
		]
	},
	lintOnSave: process.env.NODE_ENV !== "production",
	pages: {
		index: {
		  entry: "src/main.js"
		}
	},
	chainWebpack: (config) => {
		config.module
		  .rule("graphql")
		  .test(/\.gql$/)
		  .use("graphql-tag/loader")
		  .loader("graphql-tag/loader")
		  .end();
	},

	"transpileDependencies": [
		"vuetify"
	]
};
