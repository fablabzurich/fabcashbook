const { syncMembersWithBexio, importState } = require("../services/bexio/bexioSync");

const path = require("path");
const importStatePath = path.join(__dirname, "../views/importState.html");

async function performSyncMembers() {
	if (importState.isRunning) {
		console.log("Import is already running.");
		return;
	}

	importState.isRunning = true;
	try {
		const limit = 20; 
		const totalImported = await syncMembersWithBexio(limit);
        
		importState.totalImported = totalImported;
		importState.message = "Import completed successfully";
	} catch (error) {
		console.error("Error during member import:", error);
		importState.message = "Error during import";
	} finally {
		importState.isRunning = false;
	}
}
exports.performSyncMembers = performSyncMembers;

exports.syncMembers = async (req, res) => {
	// Trigger the import process without waiting for it to complete
	performSyncMembers().then(message => console.log(message));

	// Immediately return the HTML template indicating the import has started
	res.status(202).sendFile(importStatePath);
};


// Get import state
exports.getImportState = async (req, res) => {
	res.json(importState); // Return the current state
};

exports.stopImport = async (req, res) => {
	importState.isRunning = false;
	res.status(200).sendFile(importStatePath);
};

exports.showImport = async (req, res) => {
	res.status(200).sendFile(importStatePath);
};


// Add a new member
exports.addMember = async (req, res) => {
	// Logic to add a new member
};

// Update member information
exports.updateMember = async (req, res) => {
	// Logic to update member information
};

// Delete a member
exports.deleteMember = async (req, res) => {
	// Logic to delete a member
};
