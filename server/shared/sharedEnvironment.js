
function getGroupsFromEnv(envVariable) {
	const defaultMemberGroups = [{"id":6,"fee":1.0},{"id":18,"fee":0.5}];

	if (!envVariable) {
		console.log("Environment variable is empty or undefined, using default groups.");
		return defaultMemberGroups;
	}

	try {
		return JSON.parse(envVariable);
	} catch (error) {
		console.log("Error parsing environment variable, using default groups instead.");
		return defaultMemberGroups;
	}
}

// Export for Node.js and as a module for frontend use
if (typeof exports === "object") {
	module.exports = { getGroupsFromEnv };
} else if (typeof define === "function" && define.amd) {
	define(() => ({ getGroupsFromEnv }));
}
