const fs = require("fs");
const dotenv = require("dotenv");

const loadEnvConfig = (filePath) => {
	try {
		if (fs.existsSync(filePath)) {
			return dotenv.parse(fs.readFileSync(filePath));
		}
	} catch (err) {
		console.error(`Failed to load ${filePath}:`, err);
	}
	return {};
};

const configureEnvironment = () => {
	const envConfig = loadEnvConfig("./.env");
	const envLocalConfig = loadEnvConfig("./.env.local");
  
	// Merge .env and .env.local, with .env.local taking precedence
	const mergedConfig = { ...envConfig, ...envLocalConfig };
  
	// Assign the merged configurations to process.env
	Object.keys(mergedConfig).forEach((key) => {
		if (!process.env.hasOwnProperty(key)) {
			process.env[key] = mergedConfig[key];
		}
	});
};

module.exports = { configureEnvironment };
