// bexioContacts.js
const axios = require("axios");
const { apiToken, bexioBaseApiPath, headers } = require("./bexioConfig");
const { makeRequestWithRateLimiting } = require("./bexioApi");
const { delay } = require("./bexioHelpers");
const { memberGroups } = require("./bexioFeeUtils");
const { createOrderForContact } = require("./bexioOrders");
const { format } = require("date-fns");

async function fetchContacts(limit, offset) {
	const url = `${bexioBaseApiPath}/2.0/contact/search?order_by=name_1&limit=${limit}&offset=${offset}`;
	const groupIds = memberGroups.map(group => group.id);

	if (!apiToken) {
		console.log("Error: Bexio API token is not set.");
		return null;
	}

	console.log(`Bexio: ${bexioBaseApiPath}, groups: ${JSON.stringify(memberGroups)}, token: ${apiToken.substring(0, 5)}...`);

	const requestData = [
		{ field: "contact_group_ids", value: groupIds, criteria: "in" },
		{ field: "contact_type_id", value: 2, criteria: "=" }
	];

	const result = await makeRequestWithRateLimiting(url, requestData);
	if (!result) return null;

	if (result.rateLimitRemaining < 10) {
		console.log("Bexio: Approaching rate limit. Waiting for reset.");
		await delay(result.rateLimitReset * 1000);
	}

	return result.data;
}

async function sendMemberToBexio(memberData) {
	const newMemberGroupId = parseInt(process.env.BEXIO_API_GROUP_NEW_MEMBER);
	const { getGroupIdByFee } = require("./bexioFeeUtils");
	const memberFee = memberData.memberFee;
	const memberFeeGroupId = getGroupIdByFee(memberFee);
	const bexioGroups = [newMemberGroupId, memberFeeGroupId].filter(group => group !== null).join(",");
	const createdAt = new Date();

	const bexioContact = {
		contact_type_id: 2,
		name_2: memberData.firstname,
		name_1: memberData.lastname,
		address: memberData.address,
		postcode: memberData.postcode,
		city: memberData.city,
		country_id: 1,
		mail: memberData.email,
		remarks: `Created via cashtool ${format(createdAt, "dd.MM.yyyy")}`,
		contact_group_ids: bexioGroups,
		user_id: 1,
		owner_id: 1
	};

	if (process.env.BEXIO_API_CREATE_CONTACTS !== "true") {
		console.log("😜 Developer Mode! No Bexio Contact created!");
		console.log(bexioContact);
		return { ...bexioContact, id: "", nr: "DEV_MODE", mode: "developer" };
	}

	try {
		const url = `${bexioBaseApiPath}/2.0/contact`;
		const response = await axios.post(url, bexioContact, { headers });
		if (response.status >= 200 && response.status < 300) {
			const contactData = response.data;
			// Call createOrderForContact "fire and forget"
			createOrderForContact(contactData, format(createdAt, "yyyy-MM-dd"), memberFee)
				.catch(error => {
					console.error("Error in createOrderForContact:", error);
				});

			return response.data;

		} else {
			throw new Error("Failed to send member data to Bexio: " + response.status);
		}
	} catch (error) {
		console.error("Error sending data to Bexio:", error);
		throw new Error("Error connecting to Bexio API");
	}
}

module.exports = {
	fetchContacts,
	sendMemberToBexio
};
