// bexioSync.js
const { delay } = require("./bexioHelpers");
const Member = require("../../graphql/models/member");
const { fetchContacts } = require("./bexioContacts");
const { fetchMemberStatus } = require("./bexioApi");
const { calculateFeeForGroupIds, defaultMemberFee } = require("./bexioFeeUtils");

const invoicePrefix = process.env.BEXIO_API_INVOICE_TITLE_PREFIX || "MB";

async function updateMembers(membersData) {
	console.log(`updateMembers looking for invoice titles starting with "${invoicePrefix}"`);
	for (const member of membersData) {
		const {
			id: memberId,
			nr: memberNumber,
			name_1: lastname,
			name_2: firstname,
			mail: email,
			address,
			city,
			postcode,
			contact_group_ids: contactGroupIds,
			updated_at: updatedAt
		} = member;

		let memberFee = defaultMemberFee;
		if (contactGroupIds) {
			memberFee = calculateFeeForGroupIds(contactGroupIds.split(","));
		}

		const invoices = await fetchMemberStatus(memberId);
		const memberShipInvoices = invoices.filter(invoice =>
			invoice.title.trim().startsWith(invoicePrefix)
		);
		const paid =
      memberShipInvoices.length > 0 &&
      memberShipInvoices.every(invoice => parseFloat(invoice.total_remaining_payments) === 0.0);

		const latestInvoiceUpdatedAt = memberShipInvoices.reduce((latest, invoice) => {
			const currentUpdatedAt = new Date(invoice.updated_at);
			return currentUpdatedAt > latest ? currentUpdatedAt : latest;
		}, new Date(0));

		const memberToUpdate = {
			memberId,
			memberNumber,
			firstname,
			lastname,
			email,
			address,
			city,
			postcode,
			updatedAt: new Date(updatedAt),
			paid,
			paidStatusUpdatedAt: new Date(),
			invoices,
			memberFee,
			latestInvoiceUpdatedAt
		};

		console.log(`Updating member: ${memberId} ${memberNumber} ${firstname} ${lastname} | Latest Invoice: ${latestInvoiceUpdatedAt.toISOString()} | Groups: ${contactGroupIds} | Fee: ${memberFee}`);
		await Member.findOneAndUpdate({ memberId }, memberToUpdate, { upsert: true, new: true });
	}
}

let importState = {
	isRunning: false,
	totalImported: 0,
	startedAt: false,
	message: "Idle",
	duration: 0
};

async function syncMembersWithBexio(limit) {
	let offset = 0;
	importState.isRunning = true;
	importState.totalImported = 0;
	importState.message = "Import started";
	importState.startedAt = new Date();

	while (true) {
		const membersData = await fetchContacts(limit, offset);
		if (membersData && membersData.length > 0 && importState.isRunning) {
			await updateMembers(membersData);
			importState.totalImported += membersData.length;
			const now = new Date();
			importState.duration = now - importState.startedAt;
			console.log(`Total members imported: ${importState.totalImported}`);
			offset += limit;
			await delay(250);
		} else {
			console.log("No more data to import");
			break;
		}
	}
	const now = new Date();
	importState.duration = now - importState.startedAt;
	importState.isRunning = false;
	importState.message = "Import completed";
	importState.startedAt = false;

	return importState.totalImported;
}

module.exports = {
	updateMembers,
	syncMembersWithBexio,
	importState
};
