// bexioApi.js
const axios = require("axios");
const { bexioBaseApiPath, headers } = require("./bexioConfig");
const { delay } = require("./bexioHelpers");

/**
 * Makes a POST request with rate limiting handling.
 */
async function makeRequestWithRateLimiting(url, data) {
	try {
		const response = await axios.post(url, data, { headers });
		const rateLimit = response.headers["ratelimit-limit"];
		const rateLimitRemaining = response.headers["ratelimit-remaining"];
		const rateLimitReset = response.headers["ratelimit-reset"];
		console.log(`Bexio: Rate Limit: ${rateLimit}, Remaining: ${rateLimitRemaining}, Reset in: ${rateLimitReset} seconds`);
		return { data: response.data, rateLimitRemaining, rateLimitReset };
	} catch (error) {
		if (error.response && error.response.status === 429) {
			const retryAfter = error.response.headers["retry-after"];
			console.error(`Bexio: Rate limit exceeded. Retry after ${retryAfter} seconds.`);
			await delay(retryAfter * 1000);
			return makeRequestWithRateLimiting(url, data);
		} else {
			console.error("Bexio: Error making request:", error.message);
			return null;
		}
	}
}

/**
 * Fetches the invoice status for a given member.
 */
async function fetchMemberStatus(memberId) {
	const url = `${bexioBaseApiPath}/2.0/kb_invoice/search?order_by=id_desc`;
	console.log(`Bexio: ${url} memberId=${memberId}`);

	const requestData = [
		{ field: "contact_id", value: `${memberId}`, criteria: "=" }
	];

	try {
		const response = await axios.post(url, requestData, { headers });
		return response.data;
	} catch (error) {
		console.error("Error fetching invoice data from Bexio:", error);
		return null;
	}
}

module.exports = {
	makeRequestWithRateLimiting,
	fetchMemberStatus
};
