// bexioFeeUtils.js
const { getGroupsFromEnv } = require("../../shared/sharedEnvironment");

const memberGroups = getGroupsFromEnv(process.env.BEXIO_API_GROUPS);
const defaultMemberFee = process.env.DEFAULT_MEMBER_FEE || 156;

function calculateFeeForGroupId(groupId) {
	const group = memberGroups.find(g => parseInt(g.id) === parseInt(groupId));
	if (group) {
		return parseFloat((defaultMemberFee * group.fee).toFixed(2));
	}
	return null;
}

function calculateFeeForGroupIds(groupIds) {
	for (let groupId of groupIds) {
		const fee = calculateFeeForGroupId(groupId);
		if (fee !== null) {
			return fee;
		}
	}
	console.log(`No matching Group ID found in the array: ${groupIds} within ${JSON.stringify(memberGroups)}`);
	return defaultMemberFee;
}

function getGroupIdByFee(fee) {
	for (const group of memberGroups) {
		const calculatedFee = calculateFeeForGroupId(group.id);
		if (calculatedFee === fee) {
			return group.id;
		}
	}
	return null;
}

module.exports = {
	memberGroups,
	defaultMemberFee,
	calculateFeeForGroupId,
	calculateFeeForGroupIds,
	getGroupIdByFee
};
