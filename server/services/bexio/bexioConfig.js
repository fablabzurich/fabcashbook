// bexioConfig.js
const apiToken = process.env.BEXIO_API_TOKEN;
const bexioBaseApiPath = process.env.BEXIO_API_PATH || "https://api.bexio.com";

const headers = {
	"Authorization": `Bearer ${apiToken}`,
	"Accept": "application/json",
	"Content-Type": "application/json"
};

module.exports = {
	apiToken,
	bexioBaseApiPath,
	headers
};
