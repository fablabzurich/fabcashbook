// bexioOrders.js
const axios = require("axios");
const { bexioBaseApiPath, headers } = require("./bexioConfig");

/**
 * Creates an order for a given Bexio contact.
 *
 * @param {Object} memberData - Data for the member (e.g., firstname, lastname, memberFee).
 * @param {string} orderDate - The date when the order is valid (format: YYYY-MM-DD).
 * @param {number} memberFee - memberFee per year.
 * @returns {Object} - The response data from the Bexio API.
 */
async function createOrderForContact(memberData, orderDate, memberFee) {
	const title = `MB ${memberData.name_2} ${memberData.name_1} Eintritt: ${orderDate}`;
	const contactId = memberData.id;

	const membershipNormal = {
		type: "KbPositionArticle",
		amount: "1.000000",
		unit_id: 1,
		account_id: 159,
		tax_id: 16,
		text:
  "<strong>Mitgliedschaft Normaltarif</strong><br /><br />Erneuerung Mitgliedschaft <br />für ein weiteres Jahr",
		unit_price: memberFee ? memberFee.toFixed(6) : "156.000000",
		discount_in_percent: null,
		is_optional: false,
		article_id: 7
	};

	const membershipReduced = {
		type: "KbPositionArticle",
		amount: "1.000000",
		unit_id: 1,
		account_id: 159,
		tax_id: 16,
		text:
  "<strong>Mitgliedschaft Kulturlegi</strong><br /><br />Erneuerung Mitgliedschaft <br />für ein weiteres Jahr",
		unit_price: memberFee ? memberFee.toFixed(6) : "78.000000",
		discount_in_percent: null,
		is_optional: false,
		article_id: 10
	};

	const orderPayload = {
		title: title,
		contact_id: contactId,
		contact_sub_id: null,
		user_id: 1,
		pr_project_id: null,
		logopaper_id: 1,
		language_id: 1,
		bank_account_id: 1,
		currency_id: 1,
		payment_type_id: 4,
		header:
      "Liebes FabLab Mitglied<br /><br /><span>Schön, dass Du dabei bist und das FabLab unterstützt.</span>",
		footer:
      "<span>Bei Fragen kannst Du Dich gerne an den Vorstand wenden: <br /><br />accounts@zurich.fablab.ch &nbsp;</span><br /><br /><span>Freundliche Gr&uuml;sse &nbsp;&nbsp;</span><br /><span>Dein FabLab-Team</span>",
		mwst_type: 2,
		mwst_is_net: true,
		show_position_taxes: false,
		is_valid_from: orderDate,
		delivery_address_type: 0,
		api_reference: "cashTool",
		template_slug: "63ce0d7fdee686528a19904b",
		positions: [
			memberFee.toFixed(6) == "78.000000" 
				? membershipReduced 
				: membershipNormal
		]
	};


	try {
		const url = `${bexioBaseApiPath}/2.0/kb_order`;
		const response = await axios.post(url, orderPayload, { headers });
		if (response.status >= 200 && response.status < 300) {
			const orderData = response.data;
			console.log(`Order [${orderData.id}] created successfully in Bexio.`);
			const contactData = response.data;
			// Call createRepetionForOrder "fire and forget"
			createRepetionForOrder(orderData.id, orderDate)
				.catch(error => {
					console.error("Error in createRepetionForOrder:", error);
				});
			return response.data;
		} else {
			throw new Error(`Failed to create order for contactId [${contactId}]: ${response.status}`);
		}
	} catch (error) {
		console.error(`Error creating order for contactId [${contactId}] in Bexio:`, error);
		throw error;
	}
}

async function createRepetionForOrder(orderId, startDate) {
	const repetionPayload = {
		start: startDate,
		end: null,
		repetition: {
		  type: "yearly",
		  interval: 1
		}
	  };

	  try {
		const url = `${bexioBaseApiPath}/2.0/kb_order/${orderId}/repetition`;
		const response = await axios.post(url, repetionPayload, { headers });
		if (response.status >= 200 && response.status < 300) {
			console.log(`Repetition for order [${orderId}] created successfully in Bexio.`);
			return response.data;
		} else {
			throw new Error(`Failed to create repetition for order ${orderId}: ${response.status}`);
		}
	} catch (error) {
		console.error(`Error creating repetition for order ${orderId} in Bexio:`, error);
		throw error;
	}

}

module.exports = {
	createOrderForContact
};
