const { CronJob } = require("cron");

class Scheduler {
	constructor(host) {
		this.host = host;
		this.initializeJobs();
	}

	initializeJobs() {
		// Dynamically import the membersController using require
		const { performSyncMembers } = require("../controllers/membersController");

		// Use the CRON_SCHEDULE environment variable
		const cronSchedule = process.env.CRON_SCHEDULE;

		// Check if CRON_SCHEDULE is defined and not empty
		if (cronSchedule) {
			const job = new CronJob(cronSchedule, async () => {
				console.log("Running syncMembers job");
				try {
					// Directly call syncMembers here
					await performSyncMembers();
					console.log("syncMembers executed successfully.");
				} catch (error) {
					console.error("Error executing syncMembers:", error);
				}
			}, null, true);

			job.start();
			console.log("Cron job scheduled with:", cronSchedule);
		} else {
			// Log or handle the case when scheduling is deactivated due to empty CRON_SCHEDULE
			console.log("Cron scheduling is deactivated due to empty CRON_SCHEDULE.");
		}
	}
}

module.exports = Scheduler;
