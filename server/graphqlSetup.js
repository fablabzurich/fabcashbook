const { graphqlHTTP } = require("express-graphql");
const schema = require("./graphql/schema");

const graphqlSetup = (app) => {
	app.use("/graphql", graphqlHTTP((req, res) => ({
		schema: schema,
		graphiql: true,
		context: { res, req } 
	})));
};


module.exports = graphqlSetup;
