const mongoose = require("mongoose");
require("./env-config").configureEnvironment(); 

const mongooseSetup = () => {
	const dbURI = process.env.MONGODB_URL;
	mongoose.set("strictQuery", false);
	mongoose.connect(dbURI, {
		useNewUrlParser: true,
		useUnifiedTopology: true
	}).then(() => {
		console.log("Connected to MongoDB");
	}).catch(err => {
		console.error("MongoDB connection error:", err);
	});
};

module.exports = mongooseSetup;
