const { GraphQLSchema } = require("graphql");

const {RootQuery} = require("../graphql/queries/RootQuery");
const {Mutations} = require("../graphql/mutations/Mutations");

module.exports = new GraphQLSchema({
	query: RootQuery,
	mutation: Mutations
});
