// utils/stringHelper.js

function escapeRegexCharacters(str) {
	// List of special characters that need to be escaped in a regex
	// Note: "-" is included at the end to avoid creating a range inside the character set
	const specialChars = /[.*+?^${}()|[\]\\\-]/g;
	// Replace any occurrence of the special characters with the escaped version
	return str.replace(specialChars, "\\$&");
}

module.exports = {
	escapeRegexCharacters
};
