const ReceiptVersion = require("../models/receipt_version");

const addReceiptResolver = (parent, args) => {
	let receipt = new ReceiptVersion({
		id: args.id,
		date: args.date,
		labmanager: args.labmanager,
		remark: args.remark,
		basket: args.basket,
		total: args.total,
		paymentMethod: args.paymentMethod,
		member: args.member,
		uuid: args.uuid
	});
	return receipt.save();
};
module.exports = { addReceiptResolver };
