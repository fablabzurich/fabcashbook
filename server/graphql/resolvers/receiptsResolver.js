const Receipt = require("../models/receipt");
const _ = require("lodash");

const receiptsResolver = (parent, args) => {

	// TODO: find by date
	// { date: {"$gt": ISODate("2018-03-25T19:31:01.279Z")}}
	// {"date" :{
	//     "$lt" : ISODate("2018-03-16T19:31:01.229Z"),
	//     "$gt" : ISODate("2018-03-25T19:31:01.279Z")
	//  }}

	var orFilter = [];
	var andFilter = {};
	for (const key in args.input) {
		if (key === "basket") continue;
		if (key === "total") {
			const find = {};
			andFilter = args.input[key] < 0 ? {total: {$lt: 0}} : {total: {$gt: 0}};
			continue;
		};

		if (args.input[key].length > 0) {
			const find = {};
			find[key] = new RegExp(_.escapeRegExp(args.input[key]), "i");
			orFilter.push(find);
		}
	}
	if (args.input.basket) {
		let basket = args.input.basket;
		for (const key in basket) {
			const find = {};
			if (basket[key].length > 0) {
				find[`basket.${key}`] = new RegExp(_.escapeRegExp(basket[key]), "i");
				orFilter.push(find);
			}
		}
	}

	let searchFilter = {
		...(orFilter.length > 0 ? { $and: [ {$or: orFilter}] } : {}),
		...andFilter
	};

	return Receipt
		.find(searchFilter)
		.sort({ date: "desc" });
};
module.exports = { receiptsResolver };
