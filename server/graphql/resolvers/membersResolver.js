const Member = require("../models/member");

const { escapeRegexCharacters } = require("../utils/stringHelper");

const membersResolver = (parent, args) => {
	// Check if input argument is provided and construct a query accordingly
	let query = {};
	if (args.input?.search?.length > 0) {
		const searchTokens = args.input.search.split(" ").map(escapeRegexCharacters);
		const searchConditions = searchTokens.map(token => ({
			$or: [
				{ firstname: { $regex: token, $options: "i" } },
				{ lastname: { $regex: token, $options: "i" } },
				{ email: { $regex: token, $options: "i" } }
			]
		}));
		query = {
			$and: searchConditions
		};
	}

	return Member.find(query).sort({"firstname": 1});
};
module.exports = { membersResolver };
