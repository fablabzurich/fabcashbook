const Receipt = require("../models/receipt");
const _ = require("lodash");

const memberPaymentsResolver = (parent, args) => {
	var memberFilter = [
		{
			"basket.type": "Mitgliedschaft"
		}
	];
	if( args.input?.search?.length > 0 ) {
		let memberNameFilter = {
			"basket.description": {
				$regex: new RegExp(_.escapeRegExp(args.input.search), "i")
			}

		};
		memberFilter.push(memberNameFilter);
	}

	let aggregate = [
		{
			$unwind: "$basket"
		},
		{
			$match: {
				$and: memberFilter
			}
		},
		{
			$project: {
				"id": "$basket._id",
				"labmanager": "$labmanager",
				"date": "$date",
				"member": "$basket.description",
				"paymentMethod": "$paymentMethod",
				"price": "$basket.price",
				"key": "$basket.key",
				"name": "$basket.name",
				"uuid": "$uuid",
				"version": "$version"
			}
		}
	];

	return Receipt
		.aggregate(aggregate)
		.sort({ date: "desc" });
};
module.exports = { memberPaymentsResolver };
