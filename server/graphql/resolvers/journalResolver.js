const Journal = require("../models/journal");

const journalResolver = (parent, args, context, info) => {
	const journals = Journal.get(args.month, args.year);
	return {
		month: args.month,
		year: args.year,
		url: `http://${context.req.headers.host}/download/journal_${args.year}_${args.month}.csv`,
		items: journals
	};
};
module.exports = { journalResolver };
