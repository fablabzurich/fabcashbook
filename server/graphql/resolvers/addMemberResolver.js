const Member = require("../models/member");
const { sendMemberToBexio } = require("../../services/bexio/bexioContacts");

const addMemberResolver = async (parent, args) => {

	console.log("Member: args", args);
	const memberData = await sendMemberToBexio(args);
	console.log("Member: bexio contact", memberData);

	const member = new Member({
		firstname: args.firstname,
		lastname: args.lastname,
		email: args.email,
		address: args.address,
		city: args.city,
		postcode: args.postcode,
		memberFee: args.memberFee,
		paid: false,
		updatedAt: new Date(),
		memberId: memberData.id,
		memberNumber: memberData.nr
	});
	return member.save();
};
module.exports = { addMemberResolver };
