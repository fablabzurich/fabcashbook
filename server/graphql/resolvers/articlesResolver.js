const Article = require("../models/article");

const articlesResolver = (parent, args) => {
	return Article.find().sort({"sort":1});
};
module.exports = { articlesResolver };
