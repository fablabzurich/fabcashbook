const statusResolver = (parent, args) => {
	return {
		success: true,
		version: "5.0.0"
	};
};
module.exports = { statusResolver };
