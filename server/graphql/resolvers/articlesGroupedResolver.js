const Article = require("../models/article");

const articlesGroupedResolver = (parent, args) => {
	let regex = args.input.payment ? /^3/ : /^4/;

	let aggregate = [
		{
			$match: {
				konto: { $regex: regex, $options: "i" }
			}
		},
		{
			$addFields: {
				id: "$_id",
				paymentType: { $substr: ["$konto", 0, 1] }
			}
		},
		{
			$group: {
				_id: { $concat: ["$type"] },
				id: { $first: "$type" },
				sort: { $first: "$sort" },
				paymentType: { $first: "$paymentType" },
				articles: {
					$push: "$$ROOT"
				}
			}
		},
		{
			$addFields: {
				name: "$id",
				type: {
					$cond: [
						{ $eq: ["$paymentType", "3"] },
						"income",
						"expense"
					]
				}
			}
		},
		{
			$sort: {
				sort: 1
			}
		}
	];
	return Article
		.aggregate(aggregate)
		.sort({ sort: 1 });
};
module.exports = { articlesGroupedResolver };
