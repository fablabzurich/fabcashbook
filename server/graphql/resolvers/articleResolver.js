const Article = require("../models/article");

const articleResolver = (parent, args) => {
	return Article.findById(args.id);
};
module.exports = { articleResolver };
