const Balance = require("../models/balance");

const balancesResolver = (parent, args) => {
	// TODO: find by date
	// { date: {"$gt": ISODate("2018-03-25T19:31:01.279Z")}}
	// {"date" :{
	//     "$lt" : ISODate("2018-03-16T19:31:01.229Z"),
	//     "$gt" : ISODate("2018-03-25T19:31:01.279Z")
	//  }}
	return Balance.find().sort({ date: "desc" });
};
module.exports = { balancesResolver };
