const BalanceVersion = require("../models/balance_version");

const addBalanceResolver = (parent, args) => {
	let balance = new BalanceVersion({
		date: args.date,
		labmanager: args.labmanager,
		remark: args.remark,
		total: args.total,
		uuid: args.uuid
	});
	return balance.save();
};
module.exports = { addBalanceResolver };
