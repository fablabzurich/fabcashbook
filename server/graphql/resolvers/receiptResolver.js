const Receipt = require("../models/receipt");

const receiptResolver = (parent, args) => {
	return Receipt.findById(args.id);
};
module.exports = { receiptResolver };
