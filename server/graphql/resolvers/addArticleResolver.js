const Article = require("../models/article");

const addArticleResolver = (parent, args) => {
	let article = new Article({
		key: args.key,
		name: args.name,
		description: args.description,
		price: args.price,
		unit: args.unit,
		type: args.type,
		konto: args.konto,
		units: args.units
	});
	return article.save();
};
module.exports = { addArticleResolver };
