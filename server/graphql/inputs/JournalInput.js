const { GraphQLInt } = require("graphql");

const JournalInput = new GraphQLInputObjectType({
	name: "JournalInput",
	fields: () => ({
		month: { type: GraphQLInt },
		year: { type: GraphQLInt }
	})
});
module.exports = { JournalInput };
