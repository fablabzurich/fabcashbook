const { GraphQLObjectType, GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLFloat, GraphQLID, GraphQLList, GraphQLSchema, GraphQLNonNull } = require("graphql");
const { GraphQLDateTime } = require("graphql-scalars");

const MemberPaymentInput = new GraphQLInputObjectType({
	name: "MemberPaymentInput",
	fields: () => ({
		id: { type: GraphQLID },
		labmanager: { type: GraphQLString },
		date: { type: GraphQLDateTime },
		labmanager: { type: GraphQLString },
		member: { type: GraphQLString },
		paymentMethod: { type: GraphQLString },
		price: { type: GraphQLString },
		key: { type: GraphQLString },
		name: { type: GraphQLString },
		uuid: { type: GraphQLString },
		version: { type: GraphQLInt }
	})
});
module.exports = { MemberPaymentInput };
