const { GraphQLObjectType, GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLFloat, GraphQLID, GraphQLList, GraphQLSchema, GraphQLNonNull } = require("graphql");
const { GraphQLDateTime } = require("graphql-scalars");

const BalanceInput = new GraphQLInputObjectType({
	name: "BalanceInput",
	fields: () => ({
		id: { type: GraphQLID },
		labmanager: { type: GraphQLString },
		remark: { type: GraphQLString },
		date: { type: GraphQLDateTime },
		labmanager: { type: GraphQLString },
		remark: { type: GraphQLString },
		total: { type: graphql.GraphQLFloat },
		uuid: { type: GraphQLString },
		version: { type: GraphQLInt }
	})
});
module.exports = { BalanceInput };
