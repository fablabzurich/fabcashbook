const { GraphQLObjectType, GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLFloat, GraphQLID, GraphQLList, GraphQLSchema, GraphQLNonNull } = require("graphql");
const { GraphQLDateTime } = require("graphql-scalars");

const ReceiptInput = new GraphQLInputObjectType({
	name: "ReceiptInput",
	fields: () => ({
		id: { type: GraphQLID },
		labmanager: { type: GraphQLString },
		remark: { type: GraphQLString },
		basket: { type: new GraphQLList(ArticleInput) },
		date: { type: GraphQLDateTime },
		total: { type: graphql.GraphQLFloat },
		paymentMethod: { type: GraphQLString },
		member: { type: MemberInput },
		uuid: { type: GraphQLString },
		version: { type: GraphQLInt }
	})
});
module.exports = { ReceiptInput };
