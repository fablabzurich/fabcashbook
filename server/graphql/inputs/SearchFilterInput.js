const { GraphQLObjectType, GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLFloat, GraphQLID, GraphQLList, GraphQLSchema, GraphQLNonNull } = require("graphql");
const { GraphQLDateTime } = require("graphql-scalars");

const SearchFilterInput = new GraphQLInputObjectType({
	name: "SearchFilterInput",
	fields: () => ({
		search: { type: GraphQLString }
	})
});
module.exports = { SearchFilterInput };
