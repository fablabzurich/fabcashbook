const { GraphQLObjectType, GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLFloat, GraphQLID, GraphQLList, GraphQLSchema, GraphQLNonNull } = require("graphql");
const { GraphQLDateTime } = require("graphql-scalars");
const { ArticleInputDescriptionRuleType } = require("../types/ArticleInputDescriptionRuleType");

const ArticleInput = new GraphQLInputObjectType({
	name: "ArticleInput",
	fields: () => ({
		key: { type: GraphQLString },
		sort: { type: GraphQLInt },
		name: { type: GraphQLString },
		description: { type: GraphQLString },
		price: { type: GraphQLFloat },
		unit: { type: GraphQLString }, // Einheit
		type: { type: GraphQLString },
		konto: { type: GraphQLString },
		units: { type: GraphQLInt }, // Anzahl Einheiten
		descriptionRule: { type: ArticleInputDescriptionRuleType }
	})
});
module.exports = { ArticleInput };
