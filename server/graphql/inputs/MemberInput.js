const { GraphQLObjectType, GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLFloat, GraphQLID, GraphQLList, GraphQLSchema, GraphQLNonNull } = require("graphql");
const { GraphQLDateTime } = require("graphql-scalars");

const MemberInput = new GraphQLInputObjectType({
	name: "MemberInput",
	fields: () => ({
		memberNumber: { type: GraphQLString },
		firstname: { type: GraphQLString },
		lastname: { type: GraphQLString },
		email: { type: GraphQLString },
		address: { type: GraphQLString },
		city: { type: GraphQLString },
		postcode: { type: GraphQLString },
		memberFee: { type: GraphQLInt }
	})
});
module.exports = { MemberInput };
