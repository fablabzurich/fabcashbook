const { GraphQLObjectType, GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLFloat, GraphQLID, GraphQLList, GraphQLSchema, GraphQLNonNull } = require("graphql");
const { GraphQLDateTime } = require("graphql-scalars");

const ArticleGroupTypeFilterInput = new GraphQLInputObjectType({
	name: "ArticleGroupTypeFilterInput",
	fields: () => ({
		payment: { type: GraphQLBoolean }
	})
});
module.exports = { ArticleGroupTypeFilterInput };
