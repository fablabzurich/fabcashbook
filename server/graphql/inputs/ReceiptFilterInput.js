const { GraphQLObjectType, GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLFloat, GraphQLID, GraphQLList, GraphQLSchema, GraphQLNonNull } = require("graphql");
const { GraphQLDateTime } = require("graphql-scalars");
const { ArticleInput } = require("./ArticleInput");

const ReceiptFilterInput = new GraphQLInputObjectType({
	name: "ReceiptFilterInput",
	fields: () => ({
		labmanager: { type: GraphQLString },
		remark: { type: GraphQLString },
		basket: { type: ArticleInput },
		date: { type: GraphQLDateTime },
		paymentMethod: { type: GraphQLString },
		total: { type: GraphQLInt}
	})
});
module.exports = { ReceiptFilterInput };
