const { GraphQLString, GraphQLFloat, GraphQLNonNull, GraphQLList } = require("graphql");
const { GraphQLDateTime } = require("graphql-scalars");
const {ReceiptType} = require("../types/ReceiptType");
const {ArticleInput} = require("../inputs/ArticleInput");
const {MemberInput} = require("../inputs/MemberInput");
const {addReceiptResolver} = require("../resolvers/addReceiptResolver");

const addReceiptMutation = {
	type: ReceiptType,
	args: {
		date: { type: new GraphQLNonNull(GraphQLDateTime) },
		labmanager: { type: new GraphQLNonNull(GraphQLString) },
		remark: { type: GraphQLString },
		basket: { type: new GraphQLList(ArticleInput) },
		total: { type: new GraphQLNonNull(GraphQLFloat) },
		paymentMethod: { type: new GraphQLNonNull(GraphQLString) },
		member: { type: MemberInput },
		uuid: { type: GraphQLString }
	},
	resolve: addReceiptResolver
};
module.exports = { addReceiptMutation };
