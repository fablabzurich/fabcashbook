const { GraphQLString, GraphQLFloat, GraphQLNonNull } = require("graphql");
const {ArticleType} = require("../types/ArticleType");
const {addArticleResolver} = require("../resolvers/addArticleResolver");

const addArticleMutation = {
	type: ArticleType,
	args: {
		key: { type: new GraphQLNonNull(GraphQLString) },
		name: { type: new GraphQLNonNull(GraphQLString) },
		description: { type: GraphQLString },
		price: { type: new GraphQLNonNull(GraphQLFloat) },
		unit: { type: new GraphQLNonNull(GraphQLString) },
		type: { type: new GraphQLNonNull(GraphQLString) },
		konto: { type: new GraphQLNonNull(GraphQLString) }
	},
	resolve: addArticleResolver
};
module.exports = { addArticleMutation };
