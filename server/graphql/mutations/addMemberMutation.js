const { GraphQLString, GraphQLInt } = require("graphql");
const { MemberType } = require("../types/MemberType");
const {addMemberResolver} = require("../resolvers/addMemberResolver");

const addMemberMutation = {
	type: MemberType,
	args: {
		firstname: { type: GraphQLString },
		lastname: { type: GraphQLString },
		email: { type: GraphQLString },
		address: { type: GraphQLString },
		city: { type: GraphQLString },
		postcode: { type: GraphQLString },
		memberFee: { type: GraphQLInt }
	},
	resolve: addMemberResolver
};
module.exports = { addMemberMutation };
