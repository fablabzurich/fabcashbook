const { GraphQLObjectType } = require("graphql");

const { addArticleMutation } = require("./addArticleMutation");
const { addReceiptMutation } = require("./addReceiptMutation");
const { addBalanceMutation } = require("./addBalanceMutation");
const { addMemberMutation } = require("./addMemberMutation");

const Mutations = new GraphQLObjectType({
	name: "Mutations",
	fields: {
		addArticle: addArticleMutation,
		addReceipt: addReceiptMutation,
		addBalance: addBalanceMutation,
		addMember: addMemberMutation
	}
});
module.exports = { Mutations };
