const { GraphQLString, GraphQLFloat, GraphQLNonNull } = require("graphql");
const { GraphQLDateTime } = require("graphql-scalars");
const {BalanceType} = require("../types/BalanceType");
const {addBalanceResolver} = require("../resolvers/addBalanceResolver");

const addBalanceMutation = {
	type: BalanceType,
	args: {
		date: { type: new GraphQLNonNull(GraphQLDateTime) },
		labmanager: { type: new GraphQLNonNull(GraphQLString) },
		remark: { type: GraphQLString },
		total: { type: new GraphQLNonNull(GraphQLFloat) },
		uuid: { type: GraphQLString }
	},
	resolve: addBalanceResolver
};
module.exports = { addBalanceMutation };
