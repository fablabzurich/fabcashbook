const Receipt = require("./receipt");
const Balance = require("./balance");

function flattenReceipts(receipts) {
	const flattenReceipts = [];
	receipts.forEach((receipt) => {
		const receiptInfos = {
			date: receipt.date,
			labmanager: receipt.labmanager,
			paymentMethod: receipt.paymentMethod
		};
		const mappedBasketItem = receipt.basket.map((item) => {
			let kasse = "Eingang";
			let spending = 0;
			let intake = 0;
			if (item.totalPrice < 0) {
				kasse = "Ausgang";
				spending = -item.totalPrice;
			} else {
				intake = item.totalPrice;
			}
			const comment = `${item.name} ${item.description ?? ""}`.trim();
			return {
				...receiptInfos,
				key: item.key,
				type: item.type,
				konto: item.konto,
				kasse: kasse,
				comment: comment,
				intake: intake,
				spending: spending
			};
		});
		flattenReceipts.push(...mappedBasketItem);
	});
	return flattenReceipts;
}

function mapBalances(balances) {
	return balances.map(item => ({
		date: item.date,
		labmanager: item.labmanager,
		paymentMethod: "BAR",
		kasse: "Stand",
		konto: "",
		comment: "Kassenstand",
		ist: item.total
	}));
}

module.exports = {

	async get(month, year) {
		const fromDate = new Date(Date.UTC(year, month - 1));
		const toDate = new Date(Date.UTC(year, month));

		const qReceipts = Receipt
			.find()
			.sort({ date: "desc" })
			.where("date").gte(fromDate).lt(toDate);
		const qBalances = Balance
			.find()
			.sort({ date: "desc" })
			.where("date").gte(fromDate).lt(toDate);

		const [receipts, balances] = await Promise.all(
			[qReceipts.exec(), qBalances.exec()]
		);

		const journals = [...flattenReceipts(receipts), ...mapBalances(balances)];

		return journals.sort((item1, item2) => item1.date - item2.date);
	}
};
