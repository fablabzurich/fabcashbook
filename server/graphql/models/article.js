const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const articleSchema = new Schema({
	sort: Number,
	key: {
		type: String, 
		index: true
	},
	name: String,
	description: String,
	price: { 
		type: Number, 
		get: v => (v / 100).toFixed(2), 
		set: v => (((v === undefined || isNaN(v)) ? 0 : v) * 100).toFixed(0)
	},
	unit: String,
	type: String, // maschine, material, mitgliedschaft, ...
	konto: String,
	units: Number,
	descriptionRule: {
		label: String,
		placeholder: String,
		requiredMsg: String,
		regex: String
	}
});

articleSchema.virtual("totalPrice").get(function () {
	if (
		this.price === undefined || this.price === NaN ||
        this.units === undefined || this.units === NaN
	) return 0;

	return this.price * this.units;
});

module.exports = mongoose.model("Article", articleSchema);
