const mongoose = require("mongoose");
const receiptSchema = require("./receipt_schema");

module.exports = mongoose.model("Receipt", receiptSchema);
