const mongoose = require("mongoose");
const { v4: uuid } = require("uuid");
const Balance = require("./balance");
const balanceSchema = require("./balance_schema");

balanceSchema.pre("save", async function (next) {
	// read last saved version
	if( this.uuid === undefined) {
		this.uuid = uuid();
	}
	try {
		let lastVersions = await BalanceVersion
			.find({ uuid: this.uuid })
			.sort("-version")
			.limit(1)
			.exec();
		
		if (lastVersions.length) {
			this.version = lastVersions[0].version ?? 0;

		} else {
			this.version = 0;
		}
	 } catch (err) {
		throw err;
	 }
	// increase and set revison number
	this.version++;

	// lookup related balance and insert or update
	let balance = await Balance
		.findOne({uuid: this.uuid})
		.exec();

	if( !balance ) {
		balance = new Balance();
	}
	
	balance.uuid = this.uuid;
	balance.version = this.version;
	balance.date = this.date;
	balance.labmanager = this.labmanager;
	balance.remark = this.remark;
	balance.total = this.total;

	balance.save();

	// next pre
	next();
});

const BalanceVersion = mongoose.model("Balance_Version", balanceSchema);

module.exports = BalanceVersion;
