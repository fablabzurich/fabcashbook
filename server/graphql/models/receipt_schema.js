const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const article = require("./article");
const member = require("./member");

const receiptSchema = new Schema({
	date: Date,
	labmanager: String,
	remark: String,
	basket: [article.schema],
	total: { 
		type: Number, 
		get: v => (v / 100).toFixed(2), 
		set: v => (((v === undefined || isNaN(v)) ? 0 : v) * 100).toFixed(0)
	},
	paymentMethod: String,
	member: member.schema,
	uuid: String,
	hash: String,
	version: Number
});


module.exports = receiptSchema;
