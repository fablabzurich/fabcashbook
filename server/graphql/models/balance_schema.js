const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const balanceSchema = new Schema({
	date: Date,
	labmanager: String,
	remark: String,
	total: { 
		type: Number, 
		get: v => (v / 100).toFixed(2), 
		set: v => (((v === undefined || isNaN(v)) ? 0 : v) * 100).toFixed(0)
	},
	uuid: String,
	version: Number
});

module.exports = balanceSchema;
