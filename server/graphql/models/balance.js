const mongoose = require("mongoose");
const balanceSchema = require("./balance_schema");

module.exports = mongoose.model("Balance", balanceSchema);
