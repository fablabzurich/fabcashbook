const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Define the Invoice Schema
const invoiceSchema = new Schema({
	id: Number,
	document_nr: String,
	title: String,
	contact_id: Number,
	// contact_sub_id: Number,
	// user_id: Number,
	// project_id: Number,
	// logopaper_id: Number,
	// language_id: Number,
	// bank_account_id: Number,
	// currency_id: Number,
	// payment_type_id: Number,
	// header: String,
	// footer: String,
	total_gross: String,
	total_net: String,
	total_taxes: String,
	total_received_payments: String,
	total_credit_vouchers: String,
	total_remaining_payments: String,
	total: String,
	total_rounding_difference: Number,
	// mwst_type: Number,
	// mwst_is_net: Boolean,
	// show_position_taxes: Boolean,
	is_valid_from: Date,
	is_valid_to: Date,
	// contact_address: String,
	// kb_item_status_id: Number,
	reference: String,
	// api_reference: String,
	// viewed_by_client_at: Date,
	updated_at: Date,
	esr_id: Number,
	qr_invoice_id: Number
	// template_slug: String,
	// network_link: String
});

const memberSchema = new Schema({
	memberId: Number,
	memberNumber: String,
	firstname: String,
	lastname: String,
	email: String,
	address: String,
	city: String,
	postcode: String,
	updatedAt: Date,
	paid: Boolean,
	paidStatusUpdatedAt: Date,
	memberFee: Number,
	latestInvoiceUpdatedAt: Date,
	invoices: [invoiceSchema]
});

memberSchema.virtual("fullname").get(function () {
	return `${this.firstname} ${this.lastname}`;
});

memberSchema.virtual("fullnameAndEmail").get(function () {
	return `${this.firstname} ${this.lastname} (${this.email})`;
});


module.exports = mongoose.model("Member", memberSchema);
