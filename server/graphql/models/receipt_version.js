const mongoose = require("mongoose");
const { v4: uuid } = require("uuid");
const Schema = mongoose.Schema;

const Article = require("./article");
const Receipt = require("./receipt");

const receiptSchema = require("./receipt_schema");

receiptSchema.pre("save", async function (next) {
	// read last saved version
	if( this.uuid === undefined) {
		this.uuid = uuid();
	}
	try {
		let lastVersions = await ReceiptVersion
			.find({ uuid: this.uuid })
			.sort("-version")
			.limit(1)
			.exec();
		
		if (lastVersions.length) {
			this.version = lastVersions[0].version ?? 0;

		} else {
			this.version = 0;
		}
	 } catch (err) {
		throw err;
	 }
	// increase and set revison number
	this.version++;

	// lookup related receipt and insert or update
	let receipt = await Receipt
		.findOne({uuid: this.uuid})
		.exec();

	if( !receipt ) {
		receipt = new Receipt();
	}

	// duplicate articles in basket
	const basket = this.basket.map( b => {

		const newArticle = b.toObject();	// don't create Article, this caused the miss calculation 
		newArticle.price = b.price; 		// get price in base currency
		delete newArticle._id;				// remove original object id, to get a new one
		return newArticle;
	});
	receipt.uuid = this.uuid;
	receipt.version = this.version;
	receipt.date = this.date;
	receipt.labmanager = this.labmanager;
	receipt.remark = this.remark;
	receipt.total = this.total;
	receipt.paymentMethod = this.paymentMethod;
	receipt.basket = basket;
	receipt.member = this.member;
	receipt.save();

	next();
});

const ReceiptVersion = mongoose.model("Receipt_Version", receiptSchema);

module.exports = ReceiptVersion;
