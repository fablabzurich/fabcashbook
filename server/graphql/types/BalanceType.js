const { GraphQLObjectType, GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLFloat, GraphQLID, GraphQLList, GraphQLSchema, GraphQLNonNull } = require("graphql");
const { GraphQLDateTime } = require("graphql-scalars");

const BalanceType = new GraphQLObjectType({
	name: "Balance",
	fields: () => ({
		id: { type: GraphQLID },
		labmanager: { type: GraphQLString },
		remark: { type: GraphQLString },
		date: { type: GraphQLDateTime },
		total: { type: GraphQLFloat },
		uuid: { type: GraphQLString },
		version: { type: GraphQLInt }
	})
});
module.exports = { BalanceType };

