const { GraphQLObjectType, GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLFloat, GraphQLID, GraphQLList, GraphQLSchema, GraphQLNonNull } = require("graphql");
const { GraphQLDateTime } = require("graphql-scalars");

const JournalItemType = new GraphQLObjectType({
	name: "JournalItem",
	fields: () => ({
		date: { type: GraphQLDateTime },
		kasse: { type: GraphQLString },
		konto: { type: GraphQLString },
		labmanager: { type: GraphQLString },
		comment: { type: GraphQLString },
		intake: { type: GraphQLFloat },
		spending: { type: GraphQLFloat },
		paymentMethod: { type: GraphQLString },
		soll: { type: GraphQLFloat },
		ist: { type: GraphQLFloat }
	})
});
module.exports = { JournalItemType };
