const { GraphQLObjectType, GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLFloat, GraphQLID, GraphQLList, GraphQLSchema, GraphQLNonNull } = require("graphql");
const { GraphQLDateTime } = require("graphql-scalars");
const {ArticleDescriptionRuleType} = require("./ArticleDescriptionRuleType");

const ArticleType = new GraphQLObjectType({
	name: "Article",
	fields: () => ({
		id: { type: GraphQLID },
		sort: { type: GraphQLInt },
		key: { type: GraphQLString },
		name: { type: GraphQLString },
		description: { type: GraphQLString },
		price: { type: GraphQLFloat },
		unit: { type: GraphQLString }, // Einheit
		type: { type: GraphQLString },
		konto: { type: GraphQLString },
		units: { type: GraphQLInt }, // Anzahl Einheiten
		totalPrice: { type: GraphQLFloat },
		descriptionRule: { type: ArticleDescriptionRuleType }
	})
});
module.exports = { ArticleType };
