const { GraphQLObjectType, GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLFloat, GraphQLID, GraphQLList, GraphQLSchema, GraphQLNonNull } = require("graphql");
const { GraphQLDateTime } = require("graphql-scalars");
const {ArticleType} = require("./ArticleType");

const ArticleGroupType = new GraphQLObjectType({
	name: "ArticleGroup",
	fields: () => ({
		id: { type: GraphQLID },
		name: { type: GraphQLString },
		sort: { type: GraphQLInt },
		type: { type: GraphQLString },
		articles: { type: new GraphQLList(ArticleType) }
	})
});
module.exports = { ArticleGroupType };

