const { GraphQLObjectType, GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLFloat, GraphQLID, GraphQLList, GraphQLSchema, GraphQLNonNull } = require("graphql");
const { GraphQLDateTime } = require("graphql-scalars");
const { ArticleType } = require("./ArticleType");
const { MemberType } = require("./MemberType");

const ReceiptType = new GraphQLObjectType({
	name: "Receipt",
	fields: () => ({
		id: { type: GraphQLID },
		labmanager: { type: GraphQLString },
		remark: { type: GraphQLString },
		basket: { type: new GraphQLList(ArticleType) },
		date: { type: GraphQLDateTime },
		total: { type: GraphQLFloat },
		paymentMethod: { type: GraphQLString },
		member: { type: MemberType },
		uuid: { type: GraphQLString },
		version: { type: GraphQLInt }
	})
});
module.exports = { ReceiptType };
