const { GraphQLObjectType, GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLFloat, GraphQLID, GraphQLList, GraphQLSchema, GraphQLNonNull } = require("graphql");
const { GraphQLDateTime } = require("graphql-scalars");

const StatusType = new GraphQLObjectType({
	name: "Version",
	fields: () => ({
		success: { type: GraphQLBoolean },
		version: { type: GraphQLString }
	})
});
module.exports = { StatusType };
