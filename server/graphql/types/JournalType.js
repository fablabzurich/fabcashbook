const { GraphQLObjectType, GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLFloat, GraphQLID, GraphQLList, GraphQLSchema, GraphQLNonNull } = require("graphql");
const { GraphQLDateTime } = require("graphql-scalars");

const { JournalItemType } = require("./JournalItemType");

const JournalType = new GraphQLObjectType({
	name: "Journal",
	fields: () => ({
		month: { type: GraphQLInt },
		year: { type: GraphQLInt },
		url: { type: GraphQLString },
		items: { type: new GraphQLList(JournalItemType) }
	})
});
module.exports = { JournalType };
