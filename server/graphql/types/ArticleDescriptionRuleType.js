const { GraphQLObjectType, GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLFloat, GraphQLID, GraphQLList, GraphQLSchema, GraphQLNonNull } = require("graphql");
const { GraphQLDateTime } = require("graphql-scalars");

const ArticleDescriptionRuleType = new GraphQLObjectType({
	name: "ArticleDescriptionRule",
	fields: () => ({
		label: { type: GraphQLString },
		placeholder: { type: GraphQLString },
		requiredMsg: { type: GraphQLString },
		regex: { type: GraphQLString }
	})
});
module.exports = { ArticleDescriptionRuleType };
