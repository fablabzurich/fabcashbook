const { GraphQLObjectType, GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLFloat, GraphQLID, GraphQLList, GraphQLSchema, GraphQLNonNull } = require("graphql");
const { GraphQLDateTime } = require("graphql-scalars");

const MemberPaymentType = new GraphQLObjectType({
	name: "MemberPayment",
	fields: () => ({
		id: { type: GraphQLID },
		labmanager: { type: GraphQLString },
		date: { type: GraphQLDateTime },
		labmanager: { type: GraphQLString },
		member: { type: GraphQLString },
		paymentMethod: { type: GraphQLString },
		price: { type: GraphQLString },
		key: { type: GraphQLString },
		name: { type: GraphQLString },
		uuid: { type: GraphQLString },
		version: { type: GraphQLInt }
	})
});
module.exports = { MemberPaymentType };
