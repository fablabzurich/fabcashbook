const { GraphQLObjectType, GraphQLInputObjectType, GraphQLString, GraphQLInt, GraphQLBoolean, GraphQLFloat, GraphQLID, GraphQLList, GraphQLSchema, GraphQLNonNull } = require("graphql");
const { GraphQLDateTime } = require("graphql-scalars");

const MemberType = new GraphQLObjectType({
	name: "Member",
	fields: () => ({
		id: { type: GraphQLID },
		memberNumber: { type: GraphQLString },
		memberId: { type: GraphQLInt },
		firstname: { type: GraphQLString },
		lastname: { type: GraphQLString },
		fullname: { type: GraphQLString },
		fullnameAndEmail: { type: GraphQLString },
		email: { type: GraphQLString },
		address: { type: GraphQLString },
		city: { type: GraphQLString },
		postcode: { type: GraphQLString },
		updatedAt: { type: GraphQLDateTime },
		paid: { type: GraphQLBoolean },
		paidUpdatedAt: { type: GraphQLDateTime },
		memberFee: { type: GraphQLInt },
		latestInvoiceUpdatedAt: { type: GraphQLDateTime }
	})
});
module.exports = { MemberType };
