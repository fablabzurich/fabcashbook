const { GraphQLList } = require("graphql");
const { ReceiptType } = require("../types/ReceiptType");
const { ReceiptFilterInput } = require("../inputs/ReceiptFilterInput");
const { receiptsResolver } = require("../resolvers/receiptsResolver");

const receiptsQuery = {
	type: new GraphQLList(ReceiptType),
	args: {
		input: { type: ReceiptFilterInput }
	},
	resolve: receiptsResolver
};
module.exports = { receiptsQuery };
