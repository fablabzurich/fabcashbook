const { GraphQLList } = require("graphql");
const { ArticleType } = require("../types/ArticleType");
const { articlesResolver } = require("../resolvers/articlesResolver");

const articlesQuery = {
	type: new GraphQLList(ArticleType),
	args: {},
	resolve: articlesResolver
};
module.exports = { articlesQuery };
