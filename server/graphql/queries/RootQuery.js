const {GraphQLObjectType} = require("graphql");

const { statusQuery } = require("./statusQuery");
const { memberQuery } = require("./memberQuery");
const { articleQuery } = require("./articleQuery");
const { articlesQuery } = require("./articlesQuery");
const { articlesGroupedQuery } = require("./articlesGroupedQuery");
const { receiptQuery } = require("./receiptQuery");
const { receiptsQuery } = require("./receiptsQuery");
const { balancesQuery } = require("./balancesQuery");
const { journalQuery } = require("./journalQuery");
const { memberPaymentsQuery } = require("./memberPaymentsQuery");

const RootQuery = new GraphQLObjectType({
	name: "RootQueryType",
	fields: {
		status: statusQuery,
		members: memberQuery,
		article: articleQuery,
		articles: articlesQuery,
		articlesGrouped: articlesGroupedQuery,
		receipt: receiptQuery,
		receipts: receiptsQuery,
		balances: balancesQuery,
		journal: journalQuery,
		memberPayments: memberPaymentsQuery
	}
});

module.exports = { RootQuery };