const {StatusType} = require("../types/StatusType");
const {statusResolver} = require("../resolvers/statusResolver");

const statusQuery = {
	type: StatusType,
	resolve: statusResolver
};
module.exports = { statusQuery };
