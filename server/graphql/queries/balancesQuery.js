const { GraphQLList } = require("graphql");
const { BalanceType } = require("../types/BalanceType");
const { balancesResolver } = require("../resolvers/balancesResolver");

const balancesQuery = {
	type: new GraphQLList(BalanceType),
	args: {},
	resolve: balancesResolver
};
module.exports = { balancesQuery };
