const { GraphQLInt } = require("graphql");
const { JournalType } = require("../types/JournalType");
const { journalResolver } = require("../resolvers/journalResolver");
const Journal = require("../models/journal");

const journalQuery = {
	type: JournalType,
	args: {
		month: { type: GraphQLInt },
		year: { type: GraphQLInt }
	},
	resolve: journalResolver
};
module.exports = { journalQuery };
