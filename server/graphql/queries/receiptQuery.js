const { GraphQLID } = require("graphql");
const {ReceiptType} = require("../types/ReceiptType");
const {receiptResolver} = require("../resolvers/receiptResolver");

const receiptQuery = {
	type: ReceiptType,
	args: { id: { type: GraphQLID } },
	resolve: receiptResolver
};
module.exports = { receiptQuery };
