const { GraphQLList } = require("graphql");
const { MemberPaymentType } = require("../types/MemberPaymentType");
const { memberPaymentsResolver } = require("../resolvers/memberPaymentsResolver");
const { SearchFilterInput } = require("../inputs/SearchFilterInput");

const memberPaymentsQuery = {
	type: new GraphQLList(MemberPaymentType),
	args: {
		input: { type: SearchFilterInput }
	},
	resolve: memberPaymentsResolver
};
module.exports = { memberPaymentsQuery };
