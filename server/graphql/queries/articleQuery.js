const { GraphQLID } = require("graphql");
const { articleResolver } = require("../resolvers/articleResolver");
const { ArticleType } = require("../types/ArticleType");

const articleQuery = {
	type: ArticleType,
	args: { id: { type: GraphQLID } },
	resolve: articleResolver
};
module.exports = { articleQuery };
