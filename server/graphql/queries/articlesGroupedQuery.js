const { GraphQLList } = require("graphql");

const { ArticleGroupType } = require("../types/ArticleGroupType");
const { ArticleGroupTypeFilterInput } = require("../inputs/ArticleGroupTypeFilterInput");
const { articlesGroupedResolver } = require("../resolvers/articlesGroupedResolver");

const articlesGroupedQuery = {
	type: new GraphQLList(ArticleGroupType),
	args: {
		input: { 
			type: ArticleGroupTypeFilterInput, 
			defaultValue: {payment: true} 
		}
	},
	resolve: articlesGroupedResolver
};
module.exports = { articlesGroupedQuery };
