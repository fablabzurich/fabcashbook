const { GraphQLList } = require("graphql");
const { SearchFilterInput } = require("../inputs/SearchFilterInput"); 
const {membersResolver} = require("../resolvers/membersResolver");
const {MemberType} = require("../types/MemberType");

const memberQuery = {
	type: new GraphQLList(MemberType),
	args: {
		input: { type: SearchFilterInput }
	},
	resolve: membersResolver
};
module.exports = { memberQuery };
