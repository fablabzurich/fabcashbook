const express = require("express");
const router = express.Router();
const { 
	syncMembers, 
	addMember, 
	updateMember, 
	getImportState,
	stopImport,
	showImport
} = require("../controllers/membersController");

// Changed from .get to .post and added a new route for import state
router.get("/import", showImport);
router.get("/import/sync", syncMembers);
router.get("/import/state", getImportState);
router.get("/import/stop", stopImport);
router.post("/members", addMember);
router.put("/members/:id", updateMember);

module.exports = router;
