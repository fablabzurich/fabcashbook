#!/bin/bash

# checkout latest tag
# Build latest docker image with app
# tag docker image
# stop and remove existing container
# run new container



# stop script on error
set -e

# set this folder as current
cd ${0%/*}

# go one folder up
cd ../

# show current folder
pwd

# fetch latest changes from remote 
git fetch --tags --prune --force

# get latest tag
latesttag=$(git -c 'versionsort.suffix=-' \
    ls-remote --exit-code --refs --sort='version:refname' --tags origin  '*.*.*' \
    | tail --lines=1 \
    | cut -d / -f 3-4)

echo checking out ${latesttag}

# checkout latest tag
git checkout ${latesttag}

# export latest version to tag new image
AppVersion=${latesttag##*/}
AppVersion=${AppVersion:1}
export AppVersionTAG=${AppVersion}

echo "App Version ${AppVersion}"

# build node image with latest version and tag
docker-compose build

# restart docker container
docker-compose down
docker-compose up -d node

echo "done."
