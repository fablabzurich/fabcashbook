module.exports = {
	apps: [{
		name: "api",
		script: "server.js",
		watch: ["./server", "./server.js"],
		env: {
			"NODE_ENV": "development"
		},
		env_production: {
			"NODE_ENV": "production"
		}
	},
	{
		script: "serve",
		watch: ["./dist"],
		env: {
			PM2_SERVE_PATH: "./dist",
			PM2_SERVE_PORT: 8080,
			PM2_SERVE_SPA: "true",
			PM2_SERVE_HOMEPAGE: "/index.html"
		}
	}
	],
	deploy: {
		production: {
			user: "SSH_USERNAME",
			host: "SSH_HOSTMACHINE",
			ref: "origin/master",
			repo: "GIT_REPOSITORY",
			path: "DESTINATION_PATH",
			"pre-deploy-local": "",
			"post-deploy": "npm install && pm2 reload ecosystem.config.js --env production",
			"pre-setup": ""
		}
	}
};
