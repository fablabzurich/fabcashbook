module.exports = {
	client: {
		service: {
			name: "cachetool-graphql-service",
			url: "http://localhost:5001/graphql"
		},
		includes: ["./src/graphql/*.gpl"] 
	}
};
  