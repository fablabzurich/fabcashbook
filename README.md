# fabcashbook

vuejs client server application with graphql api
used to register the cash flow in FabLab Zürich

## Project setup
```
npm install
```

### Server: run api with hot-reload for development
```
npm run api:watch
```

### Client: Compiles and hot-reloads for development
```
npm run serve
```

### Client: Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Docker

### build image
You can build the nodejs image including the current version of the cashbook

```
docker-compose build
```

### get docker run

this will start all containers and you can access the App:
website: http://localhost:8080
graphql: http://localhost:5001
bexioApi: http://localhost:5001/import/status

```
docker-compose up -d
```


# helpful links

https://hackernoon.com/graphql-nested-mutations-with-apollo-small-fatigue-max-gain-1020f627ea2e

# FaLaCaBoo API
FabLab Cash Book API

# start api

```
pm2 start ecosystem.config.js
```

# Project setup 


# Using

- nodejs
- mongoose
- mongodb
- express
- graphql
- pm2


# inspired by

https://www.section.io/engineering-education/build-a-graphql-server-using-nodejs/

