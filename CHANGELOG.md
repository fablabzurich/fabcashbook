# Changelog

https://bitbucket.org/fablabzurich/fabcashbook/

## next Version (not released yet 🐛)

## Version 5.0.0 (🦋)

### improvements

- bexio sync add new member
  - add order with repetition
  - add date created remark
  - avoid duplicates on doubble click
  - 

### layout

- add member: use options for member fee selection

### fix

- new member: first and last name was swapped on bexio export

### refactoring

- splitt bexio.js in logical modules

### development

- add server side logs for env settings
- add date-fns package

## Version 4.0.2

### docker compose

- revert handling of env files

## Version 4.0.1

### fix

- allow baskets without member selection

### docker compose

- set mongodb container to version 4.4
- map local env file to container
- correct mongo mappings path
- init mongo js now is able to create collections and import data

## Version 4.0.0

### new

- bexio sync
  - scheduled hourly bexio import 
  - get payment state
  - add new member 
- bind member to payment
- autoupdate membership description

### improvements

- merge member list with payment list for validating payment status

### layout

- booking save button text updated
- new icon for member list

### development

- update handling of environment variables to change graphql port
- refactor: split graphql schema into queries, mutations, types, inputs, resolvers
- update handling of environment variables
- refactor basket store

## Version 3.0.0

### new

- articles could be edited in database now

### improvements

- remember last login name

### fix

- fix issues with precision when validating cash balance input

## Version 2.1.0

### new

- add member payments as seperate list
- cash balance: input in CHF for coins and banknotes

### layout

- cash balance: config button 
- cash balance: change order of coins

### improvements

- post booking mode: allow today
- clenup data store on logout

## Version 2.0.1

### fix

- add version to backend

## Version 2.0.0

### new

- the cash tool now supports expenses

### layout

- use icons in navigation tab
### improvements

- show payment method remarks on chackout
- show message if update is avail and Browser have to be reopened

### development

- prepare camera module to capture receipts


## Version 1.10.1

### fixes

- article description was missing and not saved in receipt

## Version 1.10.0

### fixes

- price miss calculation and rounding error

### new articles

- Drinks
- Donation


### development

- fix update script to get latest tag

## Version 1.9.4


## layout

- Article form disabled when empty
- Artikel form [ok] button disabled until price equals 0.00
- Clear Article form after adding to basket
- show warning if price is less than 1 CHF
- Add 2nd Title to checkout Dialog 
- decrease paddings increase content space


## Version 1.9.3

### fixes

- update payment method when edit receipt

### improvements

- optimize journal CSV export

### layout changes

- shorten checkout button label for edit mode

### typos

- correct changelog

### development

- get version from package.json
- add update script to fetch and build latest changes

## Version 1.9.2

### revert

 - revert changes from v1.9.1, because editing was creating new entries
 - instead add uuid and version per document directly into db


## Version 1.9.1

### fixes

 - edit is working with existing entries

## Version 1.9

### new

- edit and update cashbook entry from journal
- add save and edit history to track edit actions

### layout changes

- show clock in post booking mode
- show pencil in edit mode 

## Version 1.8.1

### improvements

- Journal search

## Version 1.8

### new

- Journal search in description and remarks so you can find members by name or email
- changelog 
- changelog link in footer

### layout changes

- logout icon added 

## Version 1.7

### improvements

- auto log out now also works when device was sleeping

## Version 1.6

### new

 - auto log out after 1 hour with 1 minute countdown

### new articles

 - workshops: CNC, Porzellan
 - shaper

### improvements

 - refactor: seperate login and log out functions
 - refactor: use correct component name
 - update to node 16 docker image
 - reset postBooking on log out

### fixes

 - fix readme api run command

## Version 1.5.1
 
### fixes

 - fix namespaces for getters
 
## Version 1.5
 
### improvements

 - reload articles when article list is shown
 - refactor: create store modules

### fixes

 - reset article validation when selecting a different item
 - fix route error msg when log out on "/" path

 
 ## Version 1.4

 ### new

 - add date picker and time buttons for post booking
 - Firstname, lastname and email are now required for new membership payment

### new articles

- Brennofen
 
### fixes

 - fix date conversion issue
 
## Version 1.3

### improvements

- use post booking date also for cash balance
 
## Version 1.2

### new

- allow post booking
- add notification 
- show errror/success messages

## Version 1.1
 
 First functional release :)

 - login with Name
 - record cash balance
 - record income from 
   - machines
   - material
   - memberships
   - workshops
- show journal
- show monthly cash book


