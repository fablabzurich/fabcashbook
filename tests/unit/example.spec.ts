import { shallowMount } from "@vue/test-utils";
import HelloLabManager from "@/components/HelloLabManager.vue";

describe("HelloLabManager.vue", () => {
	it("renders props.msg when passed", () => {
		const msg = "new message";
		const wrapper = shallowMount(HelloLabManager, {
			propsData: { msg }
		});
		expect(wrapper.text()).toMatch(msg);
	});
});
