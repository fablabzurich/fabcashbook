module.exports = {
	env: {
		node: true
	},
	plugins: ["es-beautifier"],
	extends: [
		"plugin:vue/essential",
		"@vue/typescript"
	],
	parserOptions: {
		ecmaVersion: 12,
		parser: "@typescript-eslint/parser",
		sourceType: "module"
	},
	rules: {
		"no-console": process.env.NODE_ENV === "production" ? "error" : "off",
		"no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
		quotes: ["error", "double"],
		semi: ["error", "always"],
		indent: ["error", "tab"],
		"no-multi-spaces": ["error"],
		"comma-dangle": ["error", "never"],
		"linebreak-style": "off"
	}
};
